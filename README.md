# nehv

A python script to denoise and vectorize scanned drawings.

**work in progress**

Have fun with it. A lot of fun! meow!

## Usage
```
denoise.py <infile> <outfile>
```

Currently not all image types are supported yet.

## Algorithm

0. kontrast gut machen
1. Denoise
  1. Gaussian (5x5) filter
  2. Apply treshhold (150)
2. Detect components
  1. Connected pixels
  2. Build convex structures
3. Put components into different image files
4. Vectorize each image

## License

Beerware License

